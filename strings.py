"""
Check whether the input string is palindrome.
"""


def main():
    """Check palindrome."""
    s = str(input())

    reversed = s[::-1]
    if s == reversed:
        print("yes")
    else:
        print("no")


if __name__ == "__main__":
    main()
