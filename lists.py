"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    mylist = []
    n = int(input())

    for i in range(n):
        command_line = str(input())
        args = command_line.split()

        if len(args) == 0:
            print("Not enough arguments")
        elif args[0] == "insert":
            mylist.insert(int(args[1]), int(args[2]))
        elif args[0] == "print":
            print(mylist)
        elif args[0] == "remove":
            mylist.remove(int(args[1]))
        elif args[0] == "append":
            mylist.append(int(args[1]))
        elif args[0] == "sort":
            mylist.sort()
        elif args[0] == "pop":
            mylist.pop(-1)
        elif args[0] == "reverse":
            mylist.reverse()


if __name__ == "__main__":
    main()
