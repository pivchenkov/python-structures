"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input())

    if n == 0:
        result = 1
        print(result)
    elif n > 0:
        result = 1
        for number in range(1, n + 1):
            result = result * number
        print(result)
    else:
        print("Must be >= 0")


if __name__ == "__main__":
    main()
