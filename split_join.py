def main():
    given_string = str(input())
    splitted_string = given_string.split()

    result = ""
    for word in splitted_string:
        result = result + word[::-1] + " "

    print(result[:-1])


if __name__ == "__main__":
    main()
