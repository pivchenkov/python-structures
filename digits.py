"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = int(input())

    if n >= 0:
        result = 0
        for digit in str(n):
            result = result + int(digit)
        print(result)
    else:
        print("Must be >= 0")


if __name__ == "__main__":
    main()
