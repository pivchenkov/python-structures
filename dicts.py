"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())

    result = {}
    for key in d:
        if d[key] is not None:
            result[key] = d[key]

    print(result)


if __name__ == "__main__":
    main()
